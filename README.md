# Controversies sections

Annie Rauwerda (aka [depths of wikipedia](https://twitter.com/depthsofwiki)) wanted to know what are the Wikipedia articles with the longest “Controversies” sections.
At first, I couldn’t think of a good way to answer that question
(as far as I’m aware, there’s no way to search Wikipedia specifically for headings,
or to quickly get the length of a section);
eventually, though, I thought of a solution, which is documented here.

## Approach

The general approach is to look for all articles that mention “controversies” at all, using a normal search
(as it turns out, searching [insource:controversies](https://en.wikipedia.org/w/index.php?fulltext=1&search=insource:controversies)
yields a lot fewer results than [controversies](https://en.wikipedia.org/w/index.php?fulltext=1&search=controversies),
so I went with that – 33k instead of 130k results);
to download the rendered HTML for each of those articles;
look for a “Controversies” section in it,
and then count the length of that section (in words).
This takes a while – mainly the “download HTML” step –
but it turned out to be doable within a day.

## Aside: “Controversies” articles

First, though, I noticed that some “Controversies” sections are shorter than they could be,
because they begin with a hatnote like this:

> Main article: [2022 FIFA World Cup controversies](https://en.wikipedia.org/wiki/2022_FIFA_World_Cup_controversies)

So I decided to first search for “Controversies” *articles*, which can be done more efficiently:
search for [intitle:controversies](https://en.wikipedia.org/w/index.php?fulltext=1&search=intitle:controversies),
download all the search results along with the number of words (which the search already returns),
print all of them and sort that by the number of words.
This means that we can get all the data we need out of the search API,
and don’t need to download tons of data for each article.
I popped those results in a [GitHub Gist](https://gist.github.com/lucaswerkmeister/e57dcb0a1888ee3648773ada5c809674).
(The script for that, also included in the Gist,
uses my [m3api library](https://www.npmjs.com/package/m3api), because I like using it.)

## Searching for “Controversies” sections
This is a fairly straightforward API call, but there are two interesting details.

The first is that the API, as mentioned above, returns the number of words of each article.
The “2022 FIFA World Cup” article I’d already found by hand has a “Controversies” section of ca. 3000 words,
so we know the longest “Controversies” sections have to be at least this long;
consequently, we can discard any search results that are shorter than 3000 words,
and don’t need to bother downloading their HTML.

The second is that the search API, unfortunately, returns at most 10000 results.
Since `insource:controversies` returns some 33000 results, this means we can only get about ⅓ of the results;
to get the best chance of including the results with the longest “Controversies” section, we should pick a good sorting order.
Out of the available options in the search API, I chose `incoming_links_desc`,
i.e. get the 10000 articles with the highest number of incoming links;
I thought this was most likely to return the largest articles with the most controversial subjects.
(I suppose you could improve this by repeating the search with *all* possible sort orders –
that should give you a larger subset of the 33k total results. Meh.)

I implemented this in the script [get-controversies-articles](get-controversies-articles),
and saved the results in the file [most-controversies-articles](most-controversies-articles).
(That file also includes the number of words as the first column,
since I implemented the “at least 3000 words” filter separately,
i.e. the file includes shortert articles as well.)

## Extracting “Controversies” sections from articles
The next step, and the one that took the longest to run,
was to download the HTML of each article and extract the “Controversies” section from it (if it exists).
I implemented this in the [get-controversies-section](get-controversies-section) script,
which can either be called with article titles on the command line
(`./get-controversies-section '2022 FIFA World Cup'`),
or read them from standard input (`echo '2022 FIFA World Cup' | ./get-controversies-section`);
the former mode is more convenient for testing,
the latter mode is better for doing the bulk of the work at the end.
(The other scripts I wrote, for the following steps, also follow this pattern.)
Inside the script, I used [Parsoid](https://www.mediawiki.org/wiki/Parsoid)’s [REST API](https://en.wikipedia.org/api/rest_v1/#/Page%20content/get_page_html__title_)
to download the [MediaWiki HTML](https://www.mediawiki.org/wiki/Specs/HTML/2.6.0) of each article;
this HTML is a lot nicer to work with than the HTML returned by the standard/legacy parser.
I then used the [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) library
to find a heading named exactly “Controversies”, extract the surrounding `<section>`,
and write that to an HTML file on disk.
(This way, I could repeat whatever following steps I wanted on this HTML, without downloading it again;
though in hindsight it would’ve been smarter to first save the full HTML to disk, not just a section.)
All the resulting files are also included in this repository,
as `.html` files in the [controversies-sections](controversies-sections) directory.

## Extracting plain text from HTML sections
The previous step saved HTML markup of the “Controversies” sections,
but to count the number of words, we need a plain text version.
The script for this, [section-text](section-text),
uses the Beautiful Soup library again;
it removes all the references, media, and hatnotes,
then writes the text content of the remaining HTML to a `.txt` file adjacent to the original `.html` file.
I had a look through some of the text files, and they looked like a reasonable plain text version of the article’s “Controversies” section.
Those text files are also included in the repository, again inside [controversies-sections](controversies-sections).

## Counting the number of words
This is a simple pipeline using a standard Unix utility, `wc` (word count):

```sh
wc -w controversies-sections/*.txt | sort -rn | head
```

This prints the top ten file names by word count.
(The article title is part of the file name, before the `.txt`.)

I also later added a file to the repository, [controversies-sections-words](controversies-sections-words),
containing *all* the articles by sorted word count, generated using:

```sh
wc -w controversies-sections/*.txt \
    | head -n-1 \
    | sort -rn \
    >| controversies-sections-words
```

## Bonus: Most subsections below “Controversies”
I also thought it might be interesting to find the articles with the most subsections within their overall “Controversies” section.
Since I saved the HTML of the whole section, this is fairly simple to do:
count all the heading tags within that section, excluding the “Controversies” heading itself.
The [count-subsections](count-subsections) script implements this,
and the results are in [controversies-sections-subsections](controversies-sections-subsections).
(IMHO they’re less interesting than the longest sections by number of words, though.)

## License
Since this repository includes a huge amount of English Wikipedia content,
I figured it’s easiest to put the source code and other stuff under the same license:
[CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
(But if you’d like to have any of the scripts under a more conventional software license,
let me know, I’ll be happy to dual-license them.)
