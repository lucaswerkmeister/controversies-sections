Controversies
There has been an increased interest in narcissism and narcissistic personality disorder (NPD) in the last 10 years. There are areas of substantial debate that surround the subject including:
clearly defining the difference between normal and pathological narcissism,
understanding the role of self-esteem in narcissism,
reaching a consensus on the classifications and definitions of sub-types such as "grandiose" and "vulnerable dimensions" or variants of these,
understanding what are the central versus peripheral, primary versus secondary features/characteristics of narcissism,
determining if there is consensual description,
agreeing on the etiological factors,
deciding what field or discipline narcissism should be studied by,
agreeing on how it should be assessed and measured, and
agreeing on its representation in textbooks and classification manuals.
This extent of the controversy was on public display in 2010-2013 when the committee on personality disorders for the 5th Edition (2013) of the Diagnostic and Statistical Manual of Mental Disorders recommended the removal of Narcissistic Personality from the manual. A contentious three year debate unfolded in the clinical community with one of the sharpest critics being professor John Gunderson, MD, the person who led the DSM personality disorders committee for the 4th edition of the manual.
