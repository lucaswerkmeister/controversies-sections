Controversies
On June 17, 2008, when Gimelstob was a guest on the Washington, DC, morning radio show "The Sports Junkies", he referred to French tennis player Tatiana Golovin as a "sexpot", Czech player Nicole Vaidisova as a "well developed young lady", and French player Alizé Cornet as a "little sexpot".
Also in 2008, Gimelstob told Out Magazine: "The locker room couldn’t be a more homophobic place. We’re not gay-bashing. There’s just a lot of positive normal hetero talk about pretty girls and working out and drinking beer. That’s why people want to be pro athletes!"
In 2010, Gimelstob was suspended from his Tennis Channel commentating duties for comments he made about President Barack Obama.
In 2016, Gilmelstob's wife Cary sought a restraining order against him, alleging that he “physically assaulted, harassed, verbally attacked, and stole” from her.
On May 1, 2019, Gimelstob resigned from the ATP Player Council after pressure from Stan Wawrinka and Andy Murray. He also resigned from his job at the Tennis Channel.
