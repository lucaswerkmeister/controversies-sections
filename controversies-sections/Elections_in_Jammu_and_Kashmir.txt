Controversies

Election controversies include burning prospective buildings going to be used for elections, such as what happened before the 2018 Panchayat elections in the state. The call to boycott the elections is a common feature among parties in the region, especially the regional parties and separatist groups. Election rigging has also been a criticism, especially during the 1987 Jammu and Kashmir Legislative Assembly election.
