Controversies
Drug doping and testing
Nigeria's Chika Amalaha failed a doping test and was stripped of a gold medal in the women's 53 kg weightlifting. In the women's 400 metres final, Botswana's Amantle Montsho placed fourth; she was subsequently provisionally suspended pending the results of a B sample after failing a doping test. Montsho's B sample was reported as positive on 14 August 2014.
