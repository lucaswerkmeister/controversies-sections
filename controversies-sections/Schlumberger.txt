Controversies
Radioactive sources
In 2006, a radioactive canister imported by Schlumberger was recovered in the Western Australian outback desert. The canister had been lost 
by the company's transport partner, when the improperly secured container fell off the trailer on which it was being transported.
In 2010, the Aberdeen Sheriff court fined Schlumberger Oilfield UK £300,000 for losing a radioactive source on the rig floor on the Ensco 101 mobile drilling rig in the North Sea for 4 hours.
Spills
In 2009, the Pennsylvania Department of Environmental Protection fined Chesapeake Appalachia LLC and Schlumberger Technology Corp. more than $15,500 each for a hydrochloric acid spill in February 2009 at Chesapeake's Chancellor natural gas well site in Asylum Township, Bradford County, Pennsylvania. Officials said the leak did not contaminate groundwater.
PCB contamination
In 2006, as the current owner of a facility in Pickens, South Carolina, Schlumberger agreed to pay $11.8 million to federal and state agencies for a problem caused by the previous owner, Sangamo-Weston, a capacitor manufacturing plant. The cause of the problem was from polychlorinated biphenyls (PCB) released into the environment by Sangamo-Weston from 1955 to 1987. According to the Justice Department's Environment and Natural Resources Division, an additional agreement by Schlumberger to purchase and remove dams will directly improve the Twelvemile Creek, South Carolina ecosystem and provide significant environmental benefits for the affected communities.
Deepwater Horizon

In 2010, Schlumberger was contracted to perform wireline logging on the Deepwater Horizon oil rig in the Gulf of Mexico. On the day of the explosion, the Schlumberger crew was supposed to perform a cement evaluation test, but the test was cancelled and the crew was released by BP to leave the rig on the same day. The cost-cutting decision to cancel the test is dramatized in the subsequent film by Peter Berg.
Business with Iran and Sudan
In 2015, Schlumberger pled guilty to violating U.S. sanctions related to Iran and Sudan and agreed to pay a $237 million penalty.
Sexual Harassment
In 2020, a class action lawsuit was filed against Schlumberger, alleging a culture of sexual harassment, discrimination, and retaliation. According to the company's pay-gap report for 2018, women composed a mere 5.4% of field roles and 16.2% of roles in the full company. The lawsuit alleges that males regularly documented female co-workers and flaunted them on public forums. Male colleagues would also encourage other men to break into the plaintiff's room at night and stonewall her if she did not consent to sexual activity. The lawsuit also outlines instances of "sexist and hostile comments," and one instance in which the plaintiff was groped. Women who complain to HR are told it's all just "oil field talk" and are subsequently ignored. When the plaintiff kept complaining, Schlumberger allegedly terminated the plaintiff. Women who spoke with the Texas Observer describe a culture of silence, where women stay silent for fear of being fired in the high turnover company.
