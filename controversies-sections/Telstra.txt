Controversies
On 20 March 2019, Telstra denied access to millions of Australians to the websites 4chan, 8chan, Zero Hedge, and Liveleak as a reaction to the Christchurch mosque shootings.
In May 2021, the Federal Court of Australia ordered the company to pay a $50 million fine for mistreating their Indigenous customers.
