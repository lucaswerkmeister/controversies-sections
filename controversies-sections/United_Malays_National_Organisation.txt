Controversies
1MDB scandal

Media reports from June 2018 indicated that the MACC froze bank accounts associated with UMNO, purportedly in relation to investigations into the 1MDB scandal.
Challenge to UMNO's right to exist
Every year, UMNO is obliged to hold the General Assembly to extend their rule according to the Clauses set by the Registry of Societies (RoS) Malaysia. And once every five years, UMNO is obliged to appoint their highest bodies. The last time UMNO held the election of the highest division and council level was on 19 October 2013. So the new election was supposed to be held on 19 April 2018. Their application for the postponement of the election until 19 October 2019 has to be approved by RoS in accordance with Clause 10.16. But critics have claimed that this is illegal and supposedly the existence of an UMNO organisation is banned.
Keris Incident
Umno Youth Leader Hishammuddin Hussein waded into controversy by brandishing the keris, a Malay sword's and symbol of Malay nationalism, at UMNO's 2005 annual general meeting. In response to concerns over the racial rhetoric, then Vice-President Muhyiddin Yassin said that "Although some sides were a bit extreme [this year], it is quite normal to voice feelings during the assembly." The racially provocative act was criticized by opposition politicians as well as some Chinese politicians from the Barisan Nasional coalition. In 2008, Hishammuddin conceded that the act had caused the coalition to lose support among non-Malay voters in that year's general election.
