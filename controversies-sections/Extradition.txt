Controversies
International tensions




The refusal of a country to extradite suspects or criminals to another may lead to international relations being strained. Often, the country to which extradition is refused will accuse the other country of refusing extradition for political reasons (regardless of whether or not this is justified). A case in point is that of Ira Einhorn, in which some US commentators pressured President Jacques Chirac of France, who does not intervene in legal cases, to permit extradition when the case was held up due to differences between French and American human rights law. Another long-standing example is Roman Polanski whose extradition was pursued by California for over 20 years. For a brief period he was placed under arrest in Switzerland, however subsequent legal appeals there prevented extradition.
The questions involved are often complex when the country from which suspects are to be extradited is a democratic country with a rule of law. Typically, in such countries, the final decision to extradite lies with the national executive (prime minister, president or equivalent). However, such countries typically allow extradition defendants recourse to the law, with multiple appeals. These may significantly slow down procedures. On the one hand, this may lead to unwarranted international difficulties, as the public, politicians and journalists from the requesting country will ask their executive to put pressure on the executive of the country from which extradition is to take place, while that executive may not in fact have the authority to deport the suspect or criminal on their own. On the other hand, certain delays, or the unwillingness of the local prosecution authorities to present a good extradition case before the court on behalf of the requesting state, may possibly result from the unwillingness of the country's executive to extradite.
Even though the United States has an extradition treaty with Japan, most extraditions are not successful due to Japan's domestic laws. For the United States to be successful, they must present their case for extradition to the Japanese authorities. However, certain evidence is barred from being in these proceedings such as the use of confessions, searches or electronic surveillance. In most cases involving international drug trafficking, this kind of evidence constitutes the bulk of evidence gathered in the investigation on a suspect for a drug-related charge. Therefore, this usually hinders the United States from moving forward with the extradition of a suspect.
There is at present controversy in the United Kingdom about the Extradition Act 2003, which dispenses with the need for a prima facie case for extradition. This came to a head over the extradition of the Natwest Three from the UK to the U.S., for their alleged fraudulent conduct related to Enron. Several British political leaders were heavily critical of the British government's handling of the issue.
In 2013, the United States submitted extradition requests to many nations for former National Security Agency employee Edward Snowden. It criticized Hong Kong for allowing him to leave despite an extradition request.
Extradition case of Huawei CFO Meng Wanzhou

It is a part of the China-US trade war, which is political in nature.
2019 Hong Kong extradition law protests

A proposed Hong Kong extradition law tabled in April 2019 led to one of the biggest protests in the city's history, with 1 million demonstrators joining the protests on 9 June 2019. They took place three days before the Hong Kong government planned to bypass the committee process and bring the contentious bill straight to the full legislature to hasten its approval.
The bill, which would ease extradition to Mainland China, includes 37 types of crimes. While the Beijing-friendly ruling party maintains that the proposal contains protections of the dual criminality requirement and human rights, its opponents allege that after people are surrendered to the mainland, it could charge them with some other crime and impose the death penalty (which has been abolished in Hong Kong) for that other crime. There are also concerns about the retroactive effect of the new law.
The government's proposal was amended to remove some categories after complaints from the business sector, such as "the unlawful use of computers".
Experts have noted that the legal systems of mainland China and Hong Kong follow 'different protocols' with regard to the important conditions of double criminality and non-refoulement, as well as on the matter of executive vs. judicial oversight on any extradition request.
Abductions

In some cases a state has abducted an alleged criminal from the territory of another state either after normal extradition procedures failed, or without attempting to use them. Notable cases are listed below:


Name
Year
From
To

Morton Sobell
1950
Mexico
United States

Adolf Eichmann
1960
Argentina
Israel

Antoine Argoud
1963
West Germany
France

Isang Yun
1967
West Germany
South Korea

Mordechai Vanunu
1986
Italy
Israel

Humberto Álvarez Machaín
1990
Mexico
United States

Abdullah Öcalan
1999
Kenya
Turkey

Wang Bingzhang
2002
Vietnam
China

Hassan Mustafa Osama Nasr
2003
Italy
Egypt

Rodrigo Granda
2004
Venezuela
Colombia

Konstantin Yaroshenko
2008
Liberia
United States

Dirar Abu Seesi
2011
Ukraine
Israel

Gui Minhai
2015
Thailand
China

Trịnh Xuân Thanh
2017
Germany
Vietnam

"Extraordinary rendition"

"Extraordinary rendition" is an extrajudicial procedure in which criminal suspects, generally suspected terrorists or supporters of terrorist organisations, are transferred from one country to another. The procedure differs from extradition as the purpose of the rendition is to extract information from suspects, while extradition is used to return fugitives so that they can stand trial or fulfill their sentence. The United States' Central Intelligence Agency (CIA) allegedly operates a global extraordinary rendition programme, which from 2001 to 2005 captured an estimated 150 people and transported them around the world.
The alleged US programme prompted several official investigations in Europe into alleged secret detentions and illegal international transfers involving Council of Europe member states. A June 2006 report from the Council of Europe estimated 100 people had been kidnapped by the CIA on EU territory (with the cooperation of Council of Europe members), and rendered to other countries, often after having transited through secret detention centres ("black sites") used by the CIA, some of which could be located in Europe. According to the separate European Parliament report of February 2007, the CIA has conducted 1,245 flights, many of them to destinations where suspects could face torture, in violation of article 3 of the United Nations Convention Against Torture. A large majority of the European Union Parliament endorsed the report's conclusion that many member states tolerated illegal actions by the CIA, and criticised such actions. Within days of his inauguration, President Obama signed an Executive Order opposing rendition torture and established a task force to provide recommendations about processes to prevent rendition torture.
Uyghur extradition

In June 2021, CNN reported testimonies of several Uyghurs accounting for the detention and extradition of people they knew or were related to, from the United Arab Emirates. Documents issued by the Dubai public prosecutor and viewed by CNN, showed the confirmation of China’s request for the extradition of a detained Uyghur man, Ahmad Talip, despite insufficient proof of reasons for extradition.
In 2019, UAE, along with several other Muslim nations publicly endorsed China’s Xinjiang policies, despite Beijing being accused of genocide by the US State Department. Neither Dubai authorities nor the foreign ministry of UAE respond to the several requests for comment made by CNN on the detention and extradition of Uyghurs.
