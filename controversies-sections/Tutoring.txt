Controversies
In Singapore, parents and students have positive as well as negative comments.
Tutoring centers (tuition centers) must be registered with the Singapore Ministry of Education. However, tutoring agencies are not. Instead, tutoring agencies are required to register with the Accounting and Corporate Regulatory Authority (ACRA) under the Business Registration Act. There is a history of poor compliance and consumer complaints.
