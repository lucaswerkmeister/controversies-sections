Controversies
Price-fixing
In April 2011, Unilever was fined €104 million by the European Commission for establishing a price-fixing cartel for washing powder in Europe, along with Procter & Gamble and Henkel.
In 2016, Unilever and Procter & Gamble were both fined by Autorité de la concurrence in France in 2016 for price-fixing on personal hygiene products.
Hampton Creek lawsuit
In November 2014, Unilever filed a lawsuit against rival Hampton Creek. In the suit, Unilever claimed that Hampton Creek was "seizing market share" and the losses were causing Unilever "irreparable harm." Unilever used standard of identity regulations in claiming that Hampton Creek's Just Mayo products are falsely advertised because they don't contain eggs. The Washington Post headline on the suit read "Big Food's Weird War Over The Meaning of Mayonnaise." The Los Angeles Times began its story with "Big Tobacco, Big Oil, now Big Mayo?" A Wall Street Journal writer described that "Giant corporation generates huge quantities of free advertising and brand equity for tiny rival by suing it." In December 2014, Unilever dropped the claim.
Pressuring media to promote skin whiteners
Kinita Shenoy, an editor of the Sri Lanka edition of Cosmopolitan, refused to promote skin whiteners for a brand of Unilever. Unilever put pressure on Shenoy and asked Cosmopolitan to fire her.
Violence against striking workers
In 2019, security forces hired by Unilever attacked workers that were peacefully picketing at a Unilever facility in Durban in South Africa. Workers were shot at with rubber bullets, pepper spray and paint balls while attempting to walk to their cars parked on the premises. 4 workers were seriously injured.
