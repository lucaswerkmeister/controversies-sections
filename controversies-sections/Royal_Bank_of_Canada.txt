Controversies
Discrimination
On January 15, 2007, CBC Radio reported RBC was "refusing" to open US dollar accounts for people of certain nationalities. Canadian citizens with dual citizenship in Cuba, Iran, Iraq, Myanmar, North Korea or Sudan (mostly countries with US sanctions) were affected. The US Treasury Department restricts certain foreign nationals from using the US dollar payment system to limit terrorism and money laundering after the September 11, 2001, attacks. RBC replied that compliance with such laws does not represent an endorsement by the bank and on January 17, clarified its position on the application of the US laws, specifying that "with some exceptions" it does open accounts for dual citizens of the sanctioned countries. There have also been reports that the bank had closed the accounts of some Iranian-Canadian citizens.
Environment
Environmental groups criticized RBC's financing of oil sands bitumen extraction and expansion, cumulatively issuing "more than $2.3 billion in loans and financing more than $6.9 billion in [corporate] debt between 2003 and 2007 for 13 companies including: Encana, Husky Energy, OPTI Canada, Delphi Energy, Canadian Oil Sands Trust, Northwest Upgrading, Suncor, TotalEnergies, Connacher Oil and Gas, InterPipeline and Enbridge". This opposition was due to the detrimental effect Oil Sands extraction has on the environment and human health.
A 2020 report on fossil fuel finance by the Sierra Club and Rainforest Action Network found that RBC was the fifth largest funder of fossil fuels in the world, and largest in Canada, investing over US$160 billion on fossil fuel projects since the Paris Agreement in 2015. This sparked criticism from environmental groups and climate activists.
Funding of SCO litigation on Linux
RBC invested in SCO Group during the series of court cases seeking to collect royalties from the users of Linux.
Investor protection
In 2014, the Commodity Futures Trading Commission fined RBC $35,000,000 for engaging in more than 1,000 wash sales, fictitious sales, and other non-competitive practices over a three year period. 
Mismarking
In 2007, the Royal Bank of Canada fired several traders in its corporate bond business, after another trader accused them of mismarking bonds the bank held by overpricing them, and marked down the values of the bonds and recognized $13 million of trading losses relating to the bonds. The bank said it investigated the accusations, and took remedial action. The Globe and Mail noted: "traders might have an incentive to boost [the bonds'] prices because it could have an impact on their bonuses."
Temporary foreign workers and Canadian layoffs


In April 2013, the CBC reported that the Royal Bank of Canada was indirectly hiring temporary foreign workers to replace 45 Canadian information technology workers.
The CBC reported on May 7, 2013, that during Question Period in Parliament the NDP leveled accusations against the government and then Prime Minister Stephen Harper, to which he responded that the government has been working on problems with the Temporary Foreign Worker Program for more than a year.
