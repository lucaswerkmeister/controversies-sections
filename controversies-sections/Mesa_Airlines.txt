Controversies
Flight attendants

In February 2012 the contract between flight attendants and Mesa Airlines expired and became eligible for negotiation between the Association of Flight Attendants (AFA) and Mesa.
Flight attendants have been at the core of Mesa's operations since its agreements were reached with US Airways and their United Express operation were reached and for years before. Throughout the years many of them became frustrated with stagnant wages and non-competitive retention offerings leading to its current contract negotiations which began in 2012.[citation needed]
After bankruptcy
Negotiations had stalled several times in the years following 2012 in mediation and talks directly with the company after the company had only offered negligible[clarification needed] pay increases, unaffordable[clarification needed] health insurance, and significantly fewer incentives for flight attendants with longevity compared to other regional airlines.
In January 2017, contract negotiations in front of the National Mediation Board again came to a halt. The Association of Flight Attendants (AFA), after over five years of failed negotiations,  announced that the members of the union would be coming together for a strike vote in order to either accelerate negotiations or begin using the AFA's patented[citation needed] method of striking – CHAOS – in order to interrupt operations with both American Airlines and United Airlines in anticipation that the two legacy carriers will place pressure on Mesa to offer livable wages and individual benefits comparable to other airlines.
On March 29, 2017, the AFA released a statement saying that Mesa Airlines Flight Attendants voted "Yes" to a strike vote overwhelmingly by a vote of 99.56%, the highest "yes" vote in at least 20 years for the AFA, and potentially in the history of the union since its founding in 1945.
In response Mesa Airlines CEO Jonathan Ornstein stated "We're going to continue to negotiate on good faith and come to an agreement that the company can afford", and that (It) "takes reasonableness on both parties."
Heather Stevenson, union president for Mesa, said "We're out of options" as well as "Five years of 'Please' and 'Thank you' and 'Could you do better?' hasn't done anything." She also stated "...management can choose a different outcome by seriously negotiating a contract. Mesa Airlines is an important partner in the highly-profitable American Airlines and United Airlines networks."
"Mesa Flight Attendants will not accept poverty wages", said Sara Nelson, international president of the AFA-CWA. "Enough is enough. Mesa Flight Attendants have the full backing of the 50,000 members of the Association of Flight Attendants-CWA. We are ready to do whatever it takes to achieve a fair contract."
Mesa Airlines and the AFA returned to the bargaining table during the week of April 3, 2017. While the strike vote had the intended effect, the company still offered a negligible[clarification needed] pay increase that would keep flight attendants to what they have calculated as "Industry Average". However that number was only if both parties ignore all other regional competitors financial compensation that handle the same volume as Mesa.
On April 25, 2017, members of the AFA who were not employees of Mesa carried out informational protests in Chicago, Dallas, and Phoenix to educate the public about the union's concerns such as low wages, poor health insurance, lack of progress on contract negotiations, as well as other actions of the company, and gain attention from American and United.
The AFA and Mesa Airlines met again during June 13–15 and again did not reach a resolution. The union stated "it soon became apparent the Company was not prepared to reach a deal and saw no point in bargaining until after they knew the outcome of the pilots' vote which will be counted on July 12."
The AFA and Mesa Airlines reached a Tentative Agreement on August 14 and included these changes:
Wage scale exceeding the average for Flight Attendants doing regional flights for United and American.
Increased per diem.
Block or better.
Line guarantee and increased minimum guarantee.
Long layover credit.
200% junior assignment pay.
PBS on trial basis, strong language.
Increased vacation for longevity.
Improved commuter clause.
"Me too" with pilot 401k.
Increased deadhead pay.
Jetbridge trades.
Paid KCM.
Four year duration with continued wage increases beyond the amendable date.
Pilots
On March 2, 2017, the Air Line Pilots Association, International union representing all of Mesa Airlines pilots filed a lawsuit after wages had stagnated and ceased to increase in any amount for almost ten years and the company had allegedly bypassed the Railway Labor Act by implementing bonus and incentive programs without reaching an agreement beforehand with the ALPA.
The goal of the pilots is similar to the at-the-time objective of the flight attendants, which is to be paid a standard and comparable wage to other regional airlines that can substantiate necessities such as food and shelter.
