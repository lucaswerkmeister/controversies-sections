Controversies
Harassment
Some contestants have had revenge porn containing explicit images of them posted online and have received death threats.
Suicides
Host Caroline Flack and contestants Sophie Gradon and Mike Thalassitis all died within a  20 month period by suicide. Gradon's boyfriend also killed himself about 20 days after her death.  After the suicides of Gradon and Thalassitis, concerns were raised about the pressures of reality television. Before her death, Gradon had spoken of the attacks she had received from online trolls as a result of appearing on the programme. Ian Hamilton, a senior lecturer at the University of York on addiction and mental health, described the program as one that "thrives on manufacturing conflict" and that "unfortunately it's the contestants who bear the brunt of this".
After Flack's death in February 2020, there were calls from some to cancel the programme. Questions were raised about the pressures of the show, and many drew attention to how The Jeremy Kyle Show had recently been cancelled after the suicide of a participant. In response, producers of the show provided training on handling negativity, financial management, and social media.
