Controversies

WWE
Steroid Investigation
The WWF was investigated by the Federal Government in 1991 for a steroid scandal where, reputedly, steroid use was rampant among wrestlers and in McMahon's World Bodybuilding Federation. Large names, including Hulk Hogan, gained infamy when news of their long-time steroid use was revealed. In addition, a civil lawsuit involving sexual misconduct on the part of Pat Patterson in 1992 further weakened the company. This gained great criticism to the WWF, weakening its once "family-oriented" programming.
Chris Benoit Double Murder-Suicide
WWE gained national media coverage in 2007 for the Chris Benoit murder-suicide, hypothesized to be related to brain damage resulting from multiple concussions. This incident, along with the death of Eddie Guerrero in 2005, made drug use and young deaths in the business a subject of intense controversy. The wrestling industry and the nature of the business were widely criticized for this and WWE was affected on the business-side, with the company's stock losing approximately $15 million in market value in the first week. Ratings also suffered for a short period, with Raw dropping 10% in total viewers.
