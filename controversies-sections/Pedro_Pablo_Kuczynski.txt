Controversies

First impeachment

On 15 December 2017, the Congress of the Republic initiated impeachment proceedings against Kuczynski, with the congressional opposition stating that he had lost the ″moral capacity″ to lead the country after he admitted receiving advisory fees from scandal-hit Brazilian construction company Odebrecht while he was Peru's Minister of Economy and Finance between 2004 and 2005. Kuczynski had previously denied receiving any payments from Odebrecht, but later confessed that his company, Westfield Capital Ltd, had been receiving money from Odebrecht for advisory services, while still denying that irregularities existed in the payments.
Pardon of Alberto Fujimori

On 24 December 2017, three days after surviving the impeachment vote, Kuczynski pardoned former Peruvian President Alberto Fujimori.
Second impeachment, Kenjivideos and resignation


After further scandals broke out surrounding Kuczynski, a second impeachment vote was to be held on 22 March 2018. Two days before the vote, Kuczynski stated that he would not resign and decided to face the impeachment process for a second time. The next day on 21 March 2018, a video was released of Kuczynski allies, including his lawyer and Kenji Fujimori, attempting to buy a vote against impeachment from one official.
Following the release of the video, Kuczynski presented himself before congress and officially submitted his resignation to the Congress of the Republic. Kuczynski's first vice president, Martín Vizcarra, was later named President of Peru on 23 March 2018.
